# OpenML dataset: Chernobyl-Air-Concentration

https://www.openml.org/d/43803

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
The Chernobyl disaster was a nuclear accident that occurred on 26 April 1986 at the No. 4 nuclear reactor in the Chernobyl Nuclear Power Plant, near the city of Pripyat in the north of the Ukrainian SSR. It is considered the worst nuclear disaster in history and is one of only two nuclear energy disasters rated at seven - the maximum severity - on the International Nuclear Event Scale, the other being the 2011 Fukushima Daiichi nuclear disaster in Japan.
Content
This dataset presents concentration of Iodine-131 (I-131), Caesium-134 (Cs-134) and Caesium-137 (radiocaesium, Cs-137) as anaerosol particles which were measured in specific location and date.   
On each line, following information is given:   

country code   
locality name   
lattitude (degrees.hundredths of degrees)   
longitude (degrees.hundredths of degrees)   
date (year/month/day)   
hour of end of sampling (hours:minutes)   
duration (hours.minutes)   
I-131  concentration in Bq/m3 (aerosol particles)   
Cs-134 concentration in Bq/m3 (aerosol particles)   
Cs-137 concentration in Bq/m3 (aerosol particles)   

Acknowledgements
Data extracted from REM data bank at CEC Joint Research Centre Ispra. Data was downloaded from JOINT RESEARCH CENTRE Directorate for Nuclear Safety and Security.
Inspiration
In which area the air was mostly polluted?

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43803) of an [OpenML dataset](https://www.openml.org/d/43803). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43803/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43803/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43803/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

